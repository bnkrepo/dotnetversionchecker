This is a simple library to list out all the installed .NET framework versions on a PC.
Usage is very simple as there is only one public method to consume: GetFramerowkVersion() as shown below: 


namespace DotNetVersionCheckerTest
{
    class Program
    {
        static void Main(string[] args)
        {
            DotNetVersion.GetFramerowkVersion().ForEach(version => Console.WriteLine(version.ToString()));
        }
    }
}