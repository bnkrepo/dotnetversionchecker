﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DotNetVersionChecker
{
    class Program
    {
        static void Main(string[] args)
        {
            DotNetVersion.GetFramerowkVersion().ForEach(version => Console.WriteLine(version.ToString()));
        }
    }
}
