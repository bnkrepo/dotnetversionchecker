﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DotNetVersionChecker
{
    public class DotNetVersion
    {    
        public static List<VersionData> GetFramerowkVersion()
        {
            var versionInfoData = new List<VersionData>();

            // Opens the registry key for the .NET Framework entry.
            var lmRegKey = RegistryKey.OpenRemoteBaseKey(RegistryHive.LocalMachine, "");

            using (RegistryKey ndpKey = lmRegKey.OpenSubKey(@"SOFTWARE\Microsoft\NET Framework Setup\NDP\"))
            {
                foreach (string versionKeyName in ndpKey.GetSubKeyNames())
                {
                    if (versionKeyName.StartsWith("v") && versionKeyName.IndexOf('.') != -1) // this stinrg does have '.' in it.
                    {
                        RegistryKey versionKey = ndpKey.OpenSubKey(versionKeyName);
                        string version = (string)versionKey.GetValue("Version", "");
                        string sp = versionKey.GetValue("SP", "").ToString();
                        string install = versionKey.GetValue("Install", "").ToString();

                        if (install == "1")
                        {
                            versionInfoData.Add(new VersionData()
                            {
                                Product = GetCleanVersionName(versionKeyName),
                                Version = version,
                                ServicePack = sp,
                                Type = FrameworkType.None
                            });
                        }

                        if (version != "")
                        {
                            continue;
                        }

                        foreach (string subKeyName in versionKey.GetSubKeyNames())
                        {
                            RegistryKey subKey = versionKey.OpenSubKey(subKeyName);
                            version = (string)subKey.GetValue("Version", "");

                            if (version != "")
                                sp = subKey.GetValue("SP", "").ToString();

                            install = subKey.GetValue("Install", "").ToString();

                            if (install == "1")
                            {
                                versionInfoData.Add(new VersionData()
                                {
                                    Product = GetCleanVersionName(versionKeyName),
                                    Version = version,
                                    ServicePack = sp,
                                    Type = subKeyName.CompareTo("Client") == 0 ? FrameworkType.Client : FrameworkType.Full
                                });
                            }
                        }
                    }
                    else if (versionKeyName.StartsWith("v") == true) // look for higher version
                        Get45PlusFromRegistry(versionInfoData);
                }
            }

            if (versionInfoData != null && versionInfoData.Count > 0)
                return versionInfoData.OrderBy(item => item.Product).ToList();

            return versionInfoData;
        }

        private static string GetCleanVersionName(string versionKeyName, bool bTruncateAfterSecondDecimal = true)
        {
            string cleanName = versionKeyName;

            if(string.IsNullOrEmpty(versionKeyName) == false)
            {
                if(versionKeyName.StartsWith("v") == true)
                {
                    // remove 'v' at the start
                    versionKeyName = versionKeyName.Remove(0, 1);
                }

                int index = versionKeyName.IndexOf('.');

                if (bTruncateAfterSecondDecimal == true && index != -1 && index + 1 <= versionKeyName.Length) // there is . found
                {
                    //check for next
                    index = versionKeyName.IndexOf('.', index + 1);
                    cleanName = ".Net Freamework " + ((index != -1) ? versionKeyName.Substring(0, index) : versionKeyName);
                }
                else
                    cleanName = ".Net Freamework " + versionKeyName;
            }

            return cleanName;
        }

        private static void Get45PlusFromRegistry(List<VersionData> versionInfoData)
        {
            const string subkey = @"SOFTWARE\Microsoft\NET Framework Setup\NDP\v4\";

            using (RegistryKey ndpKey = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry32).OpenSubKey(subkey))
            {
                foreach (string subKeyName in ndpKey.GetSubKeyNames())
                {
                    RegistryKey subKey = ndpKey.OpenSubKey(subKeyName);
                    var release = subKey.GetValue("Release");
                    string version = (string)subKey.GetValue("Version", "");

                    if (ndpKey != null && release != null)
                    {
                        var product = GetCleanVersionName(CheckFor45PlusVersion((int)release), false);

                        if (string.IsNullOrEmpty(product) == false)
                        {
                            versionInfoData.Add(new VersionData()
                            {
                                Product = product,
                                Version = version,
                                ServicePack = "",
                                Type = subKeyName.CompareTo("Client") == 0 ? FrameworkType.Client : FrameworkType.Full
                            });
                        }
                    }
                }
            }
        }

        // Checking the version using >= will enable forward compatibility.
        private static string CheckFor45PlusVersion(int releaseKey)
        {
            if (releaseKey >= 461808) 
                return "v4.7.2 or later";
            if (releaseKey >= 461308)
                return "v4.7.1";
            if (releaseKey >= 460798)
                return "v4.7";
            if (releaseKey >= 394802)
                return "v4.6.2";
            if (releaseKey >= 394254)
                return "v4.6.1";
            if (releaseKey >= 393295)
                return "v4.6";
            if (releaseKey >= 379893)
                return "v4.5.2";
            if (releaseKey >= 378675)
                return "v4.5.1";
            if (releaseKey >= 378389)
                return "v4.5";
            // This code should never execute. A non-null release key should mean
            // that 4.5 or later is installed.
            return "";
        }
       
    }

    public class VersionData
    {
        public string Product{ get; set; }
        public string  Version { get; set; }
        public string ServicePack { get; set; }
        public FrameworkType Type { get; set; }

        public override string ToString()
        {
            if(string.IsNullOrEmpty(ServicePack) == true)
                return string.Format("{0}, Version: {1}, Type: {2}", Product, Version, GetFrameworkTypeString(Type));
            else
                return string.Format("{0}, Version: {1}, SP{2}, Type: {3}", Product, Version, ServicePack, GetFrameworkTypeString(Type));
        }

        private string GetFrameworkTypeString(FrameworkType type)
        {
            switch (type)
            {
                case FrameworkType.Client:
                    return "Client";
                case FrameworkType.Full:
                    return "Full";
                case FrameworkType.None:
                    return "None";
            }

            return "";
        }
    }

    public enum FrameworkType
    {
        None,
        Client,
        Full
    }

}
